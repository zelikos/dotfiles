#!/bin/bash

# Batch convert recordings to DNxHR Low Bandwidth for editing in DaVinci Resolve.

EXT=.mkv
mkdir -p proxies
for f in *$EXT; do
NAME=$(echo $f | sed "s/$EXT//g")
ffmpeg -i "$f" -map 0 -c:v dnxhd -profile:v dnxhr_lb -c:a pcm_s24le proxies/"$NAME".mxf
done

