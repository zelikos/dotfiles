#!/bin/bash

# Batch convert image files to WebP for space savings.

validFormats="jpg, png"

if [[ $1 ]]
then
    case "$1" in
    "jpg")      EXT=".jpg";;
    "png")      EXT=".png";;
    *)          echo "Valid image formats: ${validFormats}";
                exit 0;;
    esac
    mkdir -p original_files
    for f in *$EXT; do
    NAME=$(echo $f | sed "s/$EXT//g")
    cwebp "$f" -q 90 -o "$NAME".webp
    mv "$f" original_files/"$f"
    done
else
    echo "Please specify an image format: ${validFormats}"
fi
