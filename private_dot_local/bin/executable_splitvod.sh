#!/bin/bash

# Split a long video (like a VOD from a stream) into 20-minute segments.
# Syntax: ./splitvod.sh <video file>

# Cuts the file extension off of the input_file to clean up the output filename
output_file=`echo $1 | cut -d. -f1`

if [[ -f $1 ]]
then
    ffmpeg -i "$1" -c copy -map 0 -segment_time 00:20:00 -f segment -reset_timestamps 1 "${output_file}_pt%03d.mp4"
else
    echo "Please enter valid filename."
fi

exit 0
