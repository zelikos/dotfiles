#!/bin/bash

# Simplifies converting recordings to DNxHR Low Bandwidth for editing in DaVinci Resolve.
# Syntax: ./convert-dnxhd.sh <video file>

# Cuts the file extension off of the selected file to clean up the output filename
output_file=`echo $1 | cut -d. -f1`

if [[ -f $1 ]]
then
    ffmpeg -i "$1" -map 0 -c:v dnxhd -profile:v dnxhr_lb -c:a pcm_s24le "${output_file}_proxy.mxf"
else
    echo "Please enter valid filename."
fi

exit 0
