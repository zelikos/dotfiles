#!/bin/bash

# A script to simplify copying of audio tracks from video files with ffmpeg.
# Currently limited to 6 tracks for automatic copying, as it is built with OBS in mind.
# Syntax: ./audioextract.sh <video file>

# What the arguments entered represent
input_file=$1
# Cuts the file extension off of the input_file to clean up the output filename
output_file=`echo "${input_file}" | cut -d. -f1`

if [[ -f $1 ]]
then
    #Takes input on which tracks to copy to a separate file.
    echo "Please select track # to copy, or press Enter for all."
    read track
    case $track in
        ''|*[!0-9]*) echo "Copying audio tracks 1-6 to separate files...";
                     ffmpeg -loglevel warning -i "$1" -map 0:1 -vn "${output_file} Track #1.wav";
                     ffmpeg -loglevel warning -i "$1" -map 0:2 -vn "${output_file} Track #2.wav";
                     ffmpeg -loglevel warning -i "$1" -map 0:3 -vn "${output_file} Track #3.wav";
                     ffmpeg -loglevel warning -i "$1" -map 0:4 -vn "${output_file} Track #4.wav";
                     ffmpeg -loglevel warning -i "$1" -map 0:5 -vn "${output_file} Track #5.wav";
                     ffmpeg -loglevel warning -i "$1" -map 0:6 -vn "${output_file} Track #6.wav";;
        *) echo "Copying track $track to separate file...";
        ffmpeg -loglevel warning -i "$1" -map 0:$track -vn "${output_file} Track #$track.wav";;
    esac
    echo "Operation Complete."
else
    echo "Please enter valid filename."
fi

exit 0
