#!/bin/bash

# Batch convert Twitch VODs to DNxHR Low Bandwidth for editing in DaVinci Resolve.

EXT=.mp4
mkdir -p proxies
for f in *$EXT; do
NAME=$(echo $f | sed "s/$EXT//g")
ffmpeg -i "$f" -map 0 -c:v dnxhd -profile:v dnxhr_lb -c:a pcm_s24le proxies/"$NAME".mov
done

