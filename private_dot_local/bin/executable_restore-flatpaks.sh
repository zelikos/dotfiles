#!/bin/bash
while IFS= read -r line || [[ -n "$line" ]]; do
    flatpak install -y --noninteractive --user --or-update flathub $line
done < "$1"
