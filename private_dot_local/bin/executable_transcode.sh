#!/bin/bash

# A script to simplify transcoding to a few different video codecs with ffmpeg,
# with specific ffmpeg settings for Akzel's usage, i.e. prores proxy,
# distributing to video content platforms (YouTube, Twitch, PeerTube).
# Transcodes to a select few codecs; see validCodecs below.
# Syntax: ./transcode.sh <video file>

# What the arguments entered represent
input_file=$1
# Cuts the file extension off of the input_file to clean up the output filename
output_file=`echo "${input_file}" | cut -d. -f1`

validCodecs="h264, h265, prores, xvid"

if [[ -f $1 ]]
then
    #Takes input on which tracks to copy to a separate file.
    echo "Please select video codec: ${validCodecs}"
    read codec
    case "$codec" in
        "h264")     echo "Transcoding $1 to h264...";
                    ffmpeg -loglevel warning -i "$1" -c:v libx264 -c:a aac -crf 18 -preset medium "${output_file}_h264.mp4";;
        "h265")     echo "Transcoding $1 to h265...";
                    ffmpeg -loglevel warning -i "$1" -c:v libx265 -c:a aac -b:a 192k -x265-params crf=23 -preset medium "${output_file}_h265.mp4";;
        "prores")   echo "Transcoding $1 to prores (proxy)...";
                    ffmpeg -loglevel warning -i "$1" -c:v prores -profile:v 0 -c:a pcm_s24le "${output_file}_proxy.mov";;
        "xvid")      echo "Transcoding $1 to xvid...";
                    ffmpeg -loglevel warning -i "$1" -c:v libxvid -q:v 2 -c:a pcm_s24le "${output_file}_xvid.mov";;
        *)          echo "Valid codecs: ${validCodecs}";
                    exit 0;;
    esac
    echo "Operation Complete."
else
    echo "Please enter valid filename."
fi

exit 0
